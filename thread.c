#include<stdio.h>
#include<pthread.h>
//#include<math.h>

#define N 103
int S=0;
//int N;
int sir[N];


struct st{
    int s;
    int start, end;
};

void* add(void *p){
    int i;
    struct st* pp = (struct st*)p;
    
    pp->s=0;
    for(i=pp->start; i < pp->end; i++) {
		pp->s += sir[i];
    }
    printf("\n The partial sum is: %d", pp->s);
    S+=pp->s;
}

int main(){ 
    pthread_t th[4]; 
    int j,ret;      
    struct st a[4];
      
    //printf("N= ");
    //scanf("%d", &N);
      
    for(j=0; j<N; j++){
		sir[j]=j+1;
    }
   
    for(j = 0; j < 3; j++){ 
	
		a[j].start = (N/4)*j;
		a[j].end = (N/4)*(j+1);
	
		pthread_create(&th[j], NULL, add, &a[j]); 
	  
    }   
    a[3].start = 75;
    a[3].end = N;
      
    pthread_create(&th[3], NULL, add, &a[3]);
	for(j = 0; j < 4; j++){ 
		pthread_join(th[j], NULL); 
	}   
	
    printf("\n The value of S is %d\n", S); 
}  
