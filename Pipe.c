// Să se adune elementele unui vector folosind N procesoare. Folositi numai procese.
#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include<stdlib.h>

int a[20];
int nr = 4;
int n = 20;
int S = 0;

void childProc(int readPipe, int writePipe) {
    int i,j;
    int s;

    read(readPipe, &i, sizeof(int));
    for(j=(n/nr)*i + 1; j<(n/nr)*(i+1) + 1; j++) {
        s += j;
    }
    write(writePipe, &s, sizeof(int));
    printf("slave %d has finished!\n", i);
    exit(1);
}
void parentProc(int readPipe, int writePipe, int i) {
    int rez;
    write(writePipe, &i, sizeof(int));
    read(readPipe, &rez, sizeof(int));

    S = S + rez;
    printf("suma %d = %d\n", i, rez);
}
int main() {
  int firstPipe[2];
  int secondPipe[2];
  int pid;
  int j;

  if (pipe(firstPipe)) {
    printf("Nu am putut crea conducta!\n");
    exit(-1);
  }
  if (pipe(secondPipe)) {
    printf("Nu am putut crea conducta!\n");
    exit(-1);
  }
  for(j=0; j<nr; j++) {
  pid = fork();
  if (pid) {
    if (pid == -1) {
      printf("Nu am reusit crearea procesului copil %d.\n",j);
      exit(-1);
   }
    parentProc(secondPipe[0], firstPipe[1],j);
  } else {
    childProc(firstPipe[0], secondPipe[1]);
  }
  }
  wait(NULL);
  printf("\nSuma finala este : %d \n",S);
}
