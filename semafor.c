#include<stdio.h>
#include<pthread.h>
int nr_cautat = 3;
int V[9] = {2, 3, 1, 4, 3, 3, 6, 7, 1};
int poz[9];
int ind = 0;
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;

void *cauta(void *arg){
    int i;
    int val = *(int *)arg;
    for(i=3*val; i < 3*(val+1); i++) {
		if(V[i] == nr_cautat){
			pthread_mutex_lock(&m);
			poz[ind] = i;
			ind++;
			pthread_mutex_unlock(&m);
		}
    }
}

int main(){ 
    pthread_t th[3]; 
    int i;
    int x[3];     
   
    for(i = 0; i < 3; i++){
		x[i] = i;
		pthread_create(&th[i], NULL, cauta, &x[i]); 
    }
	for(i = 0; i < 3; i++){ 
		pthread_join(th[i], NULL); 
	} 
	
    printf("\n Positions of %d are: \n", nr_cautat);
    for(i=0; i<ind; i++) {
		printf("%d ",poz[i]);
    }
}  
