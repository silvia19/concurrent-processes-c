// Să se adune elementele unui vector folosind N procesoare.

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int MAX_THREAD = 3;
int v[9] = {2,3,1,4,3,2,3,1,2};
int s = 0;

// Initialize mutex
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;

void *sum (int *arg) {
    int *p = arg;
    int i, start, end;

    // Takes 3 threads at once
    start = *p*3;
    end = start + 3;

    for(i = start; i < end; i++) {
        // Lock mutex
        pthread_mutex_lock(&m);
        s += v[i];
        // Unlock mutex
        pthread_mutex_unlock(&m);
    }
    pthread_exit(NULL);
}

int main()
{
    int i, j, start, tids[MAX_THREAD];
    pthread_t threads[MAX_TRHREAD];

    // Create mutex
    for(i = 0; i < MAX_THREAD; i++) {
        tids[i] = i;
        pthread_create(&threads[i], NULL, cauta, %tids[i]);
    }

    // Join mutex
    for(i = 0; i < MAX_THREAD; i++)
        pthread_join(threads[i], NULL);

    // Check position
    for(i = 0; i < MAX_THREAD; i++) {
        printf("Suma elementelor este %d.", s);
    }
}
