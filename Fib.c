#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#define COMMAND_SIZE 20

char fib[] = "fibonacci";
char add[] = "add";
char end[] = "end";

// Fibonacci
int fibonacci(int n) {
  if (n < 2) {
    return n;
  }
  return fibonacci(n -1) + fibonacci(n - 2);
}

void childProc(int readPipe, int writePipe) {
  char message[20];
  int a, b;
  do {
    read(readPipe, message, COMMAND_SIZE);
    printf("primit %s\n", message);
    if (strcmp(message, add) == 0) {
      read(readPipe, &a, sizeof(a));
      read(readPipe, &b, sizeof(b));
      a += b;
      write(writePipe, &a, sizeof(a));

    } else if (strcmp(message, fib) == 0){
      read(readPipe, &a, sizeof(int));
      b = fibonacci(a);
      write(writePipe, &b, sizeof(int));
    }

  } while (strcmp(message, end));
  printf("the wonder child has finished!\n");
  exit(1);
}

void parentProc(int readPipe, int writePipe) {
    int a, b, rez;
    write(writePipe, fib, COMMAND_SIZE);
    a = 3;
    write(writePipe, &a, sizeof(int));
    read(readPipe, &rez, sizeof(int));
    printf("%s(%d) = %d\n", fib, a, rez);
    a = 2;
    b = 5;
    write(writePipe, add, COMMAND_SIZE);
    write(writePipe, &a, sizeof(int));
    write(writePipe, &b, sizeof(int));
    read(readPipe, &rez, sizeof(int));
    printf("%d + %d = %d\n", a, b, rez);

    write(writePipe, end, strlen(end) + 1);
}

int main() {

  int firstPipe[2];
  int secondPipe[2];
  int pid;

  if (pipe(firstPipe)) {
    printf("Nu am putut crea conducta!\n");
    exit(-1);
  }
  if (pipe(secondPipe)) {
    printf("Nu am putut crea conducta!\n");
    exit(-1);
  }

  pid = fork();

  if (pid) {
    if (pid == -1) {
      printf("Nu am reusit crearea procesului copil.\n");
      exit(-1);
    }

    parentProc(secondPipe[0], firstPipe[1]);

  } else {
    childProc(firstPipe[0], secondPipe[1]);
  }
  wait(NULL);
  printf("Procesul principal gata!\n");
}

