#include<stdio.h>
#include<pthread.h>

int V[9] = {2, 3, 1, 4, 3, 3, 6, 7, 1};
int sortat[10];
int ind = 0;
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;

void *cauta(void *arg){
    int i,j,k;
    int val = *(int *)arg;
    for(i=3*val; i < 3*(val+1); i++) {
	
		if(ind==0){  
			pthread_mutex_lock(&m);
			sortat[ind] = V[i];
			ind++;
			
			pthread_mutex_unlock(&m);
			
		} else{
			j=0;
			
			pthread_mutex_lock(&m);
			 
			while(V[i] > sortat[j] && j < ind) 
				j++;
		   
			for(k= ind; k>j; k--){
				sortat[k] = sortat[k-1];
			}
			
			sortat[k]=V[i];
			pthread_mutex_unlock(&m);
			ind++;
		}		
	}
}

int main(){ 
    pthread_t th[3]; 
    int i;
    int x[3];     
   
    for(i = 0; i < 3; i++){
		x[i] = i;
		pthread_create(&th[i], NULL, cauta, &x[i]); 
    }
	
	for(i = 0; i < 3; i++){ 
		pthread_join(th[i], NULL); 
    } 
	
    printf("\n Sorted vector is: \n");
    for(i=0; i<9; i++) {
		printf("%d ",sortat[i]);
    }
}